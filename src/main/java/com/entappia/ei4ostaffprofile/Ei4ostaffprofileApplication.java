package com.entappia.ei4ostaffprofile;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.entappia.ei4ostaffprofile.dbmodels.FirmwareSettings;
import com.entappia.ei4ostaffprofile.constants.AppConstants;
import com.entappia.ei4ostaffprofile.dbmodels.Tags;
import com.entappia.ei4ostaffprofile.repository.TagsRepository;
import com.entappia.ei4ostaffprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4ostaffprofile.dbmodels.CampusDetails;
import com.entappia.ei4ostaffprofile.quuppa.QuuppaApiService;
import com.entappia.ei4ostaffprofile.repository.CampusDetailsRepository;
import com.entappia.ei4ostaffprofile.repository.FirmwareSettingsRepository;
import com.entappia.ei4ostaffprofile.utils.LogEvents;
import com.entappia.ei4ostaffprofile.utils.Utils;

@SpringBootApplication
@EnableScheduling
public class Ei4ostaffprofileApplication {

	@Autowired
	TagsRepository tagsRepository;
	
	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private LogEvents logEvents;
	
	@Autowired
	private FirmwareSettingsRepository firmwareSettingsRepository;

	public static void main(String[] args) {
		SpringApplication.run(Ei4ostaffprofileApplication.class, args);
	}
	
	@PostConstruct
	void getProjectInfo() {

		ResourceBundle bundle = ResourceBundle.getBundle("application");

		FirmwareSettings firmwareSettings = firmwareSettingsRepository.findByApplication("staff");

		if (firmwareSettings == null) {
			firmwareSettings = new FirmwareSettings();
			firmwareSettings.setApplication("staff");
			firmwareSettings.setDownloadedVersion("");
			firmwareSettings.setCreatedDate(new Date());
			firmwareSettings.setModifiedDate(new Date());
			firmwareSettings.setVersion(bundle.getString("staffversion"));
		}else {

			String versionInDB = firmwareSettings.getVersion();

			if(versionInDB == null || !versionInDB.equals(bundle.getString("staffversion"))) {

				firmwareSettings.setVersion(bundle.getString("staffversion"));
				firmwareSettings.setCreatedDate(new Date());
				firmwareSettings.setModifiedDate(new Date());
			}
			if( firmwareSettings.getDownloadedVersion() == null || firmwareSettings.getDownloadedVersion().isEmpty()) {
				firmwareSettings.setDownloadedVersion("");
			}
			if( firmwareSettings.getCreatedDate() == null || firmwareSettings.getCreatedDate().toString().isEmpty()) {
				firmwareSettings.setCreatedDate(new Date());
			}

			if( firmwareSettings.getModifiedDate() == null || firmwareSettings.getModifiedDate().toString().isEmpty()) {
				firmwareSettings.setModifiedDate(new Date());
			}


		}



		firmwareSettingsRepository.save(firmwareSettings);

		try {

			//TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Runnable() {

				public void run() {
					CompletableFuture<JSONObject> projectInfoCompletableFuture;
					try {
						projectInfoCompletableFuture = quuppaApiService.getProjectInfo();

						CompletableFuture.allOf(projectInfoCompletableFuture).join();

						JSONObject projectsInfoJsonObject = null;
						if (projectInfoCompletableFuture.isDone()) {
							projectsInfoJsonObject = projectInfoCompletableFuture.get();

							if (projectsInfoJsonObject != null) {

								String status = projectsInfoJsonObject.optString("status");

								if (!Utils.isEmptyString(status) && status.equals("success")) {

									HashMap<String, Object> data = new HashMap<>();

									JSONObject projectInfoJsonObject = projectsInfoJsonObject
											.optJSONObject("backgroundImages");

									if (projectInfoJsonObject != null) {
										double widthMeter = projectInfoJsonObject.getDouble("widthMeter");
										double heightMeter = projectInfoJsonObject.getDouble("heightMeter");

										double metersPerPixelX = projectInfoJsonObject.getDouble("metersPerPixelX");
										double metersPerPixelY = projectInfoJsonObject.getDouble("metersPerPixelY");

										double origoX = projectInfoJsonObject.getDouble("origoX");
										double origoY = projectInfoJsonObject.getDouble("origoY");

										;

										CampusDetails campusDetails = campusDetailsRepository.findByCampusId(1);
										if (campusDetails != null) {
											campusDetails.setMetersPerPixelX(metersPerPixelX);
											campusDetails.setMetersPerPixelY(metersPerPixelY);
											campusDetails.setOriginX(origoX);
											campusDetails.setOriginY(origoY);
											campusDetailsRepository.save(campusDetails);
											logEvents.addLogs(LogEventType.EVENT, "ProjectInfo",
													"Get Project Info-StaffprofileApplication",
													"Campus details updated");

										}
									}

								} else {

									String message = projectsInfoJsonObject.optString("message");
									logEvents.addLogs(LogEventType.ERROR, "ProjectInfo", "Get Project Info-StaffprofileApplication",
											message);
								}

							}
						}

					} catch (InterruptedException e) {
						logEvents.addLogs(LogEventType.ERROR, "ProjectInfo", "Get Project Info-StaffprofileApplication",
								e.getMessage());
						e.printStackTrace();
					} catch (ExecutionException e) {
						logEvents.addLogs(LogEventType.ERROR, "ProjectInfo", "Get Project Info-StaffprofileApplication",
								e.getMessage());
						e.printStackTrace();
					} catch (JSONException e) {
						logEvents.addLogs(LogEventType.ERROR, "ProjectInfo", "Get Project Info-StaffprofileApplication",
								e.getMessage());
						e.printStackTrace();
					}
				}
			});
			executorService.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
