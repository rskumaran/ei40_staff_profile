package com.entappia.ei4ostaffprofile.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.ProfileConfiguration;

@Repository
public interface ProfileConfigurationRepository extends CrudRepository<ProfileConfiguration, String> {
	ProfileConfiguration findByOrganization(String organization);
	
	@Query(value = "SELECT * FROM profile_configuration limit 0, 1", nativeQuery = true)
	ProfileConfiguration selectTopRecord();
	
	@Modifying
	@Transactional
	@Query(value = "truncate table Profile_Configuration", nativeQuery = true)
	void truncateMyTable();
  }
 