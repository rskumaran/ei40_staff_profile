package com.entappia.ei4ostaffprofile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.Configuration;
 
@Repository
public interface ConfigurationRepository extends CrudRepository<Configuration, Integer> {

}
