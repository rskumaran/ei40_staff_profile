package com.entappia.ei4ostaffprofile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.ZoneDetails;

@Repository
public interface ZoneRepository extends CrudRepository<ZoneDetails, Integer> {

	ZoneDetails findByName(String mapName);

	ZoneDetails findById(int id);

	List<ZoneDetails> findAll();

	@Query(value = "select * from zone_details where name=:name and active=:active", nativeQuery = true)
	ZoneDetails findZoneDetails(@Param("name") String name, @Param("active") boolean active);
	
	@Query(value = "select * from zone_details where name=:name and id !=:id and active=:active", nativeQuery = true)
	ZoneDetails findZoneDetails(@Param("name") String name, @Param("id") int id, @Param("active") boolean active);
}
