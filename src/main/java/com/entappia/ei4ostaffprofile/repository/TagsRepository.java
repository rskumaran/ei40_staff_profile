package com.entappia.ei4ostaffprofile.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.Tags;

@Repository
public interface TagsRepository extends CrudRepository<Tags, Integer> {
	Tags findByTagId(String tagId);
	 
	Tags findByMacId(String macId);
	
	@Modifying
	@Transactional
	@Query(value = "update tags set restart = 1 where mac_id=:macId", nativeQuery = true)
	void setRestartFlag(String macId);
	
	
	@Query(value = "select * from tags where restart = 0 and status not like 'free' or status not like 'assigned')", nativeQuery = true)
	List<Tags> findNotFreeAndNotAssignedTags();
	
	//@Query(value = "select * from tags where restart = 0 and (status like 'lost' or status like 'blocked')", nativeQuery = true)
	@Query(value = "select * from tags where (status like 'lost' or status like 'blocked')", nativeQuery = true)
	List<Tags> findLostAndBlockedTags();
	
	@Query(value = "select * from tags where mac=:mac and tag_id!=:tagid", nativeQuery = true)
	Tags checkMacId(@Param("mac") String mac,
			@Param("tagid") String tagid);
	
	List<Tags> findAll();

	@Query(value = "select * from tags where status = :status and ( type like 'Employee' or type like 'vendor' or type like 'contractor' or type like 'visitor')", nativeQuery = true)
	Iterable<Tags> selectVisitorAndEmployeeTags(String status);
	
	@Query(value = "select * from tags where status = :status and type = :type", nativeQuery = true)
	Iterable<Tags> selectTags(String status, String type);
}
