package com.entappia.ei4ostaffprofile.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.Staff;


@Repository
public interface StaffRepository  extends CrudRepository<Staff, Integer> {
	
	@Query(value = "select * from staff where emp_id=:empId", nativeQuery = true)
	Staff getStaffByempId(@Param("empId") String empId);

}
