package com.entappia.ei4ostaffprofile.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.VisitorAllocation;

@Repository
public interface VisitorAllocationRepository extends CrudRepository<VisitorAllocation, Integer> {
	
	@Query(value = "SELECT * FROM visitor_allocation where visitor_id=:vistorId and mac_id =:macId and status='Valid';", nativeQuery = true)
	VisitorAllocation findVisitorAllocation(@Param("vistorId") int vistorId, @Param("macId") String macId);

}
