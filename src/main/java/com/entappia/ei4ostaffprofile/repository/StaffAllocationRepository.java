package com.entappia.ei4ostaffprofile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.StaffAllocation;

@Repository
public interface StaffAllocationRepository extends CrudRepository<StaffAllocation, Integer> {

}
