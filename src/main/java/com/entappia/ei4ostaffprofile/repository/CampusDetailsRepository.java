package com.entappia.ei4ostaffprofile.repository;
 
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ostaffprofile.dbmodels.CampusDetails;
 

@Repository
public interface CampusDetailsRepository extends CrudRepository<CampusDetails, Integer>{

	CampusDetails findByCampusId(long campusId);
	List<CampusDetails> findAll();
}
