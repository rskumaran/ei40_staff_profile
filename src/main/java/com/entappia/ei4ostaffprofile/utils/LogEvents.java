package com.entappia.ei4ostaffprofile.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entappia.ei4ostaffprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4ostaffprofile.dbmodels.StaffLogs;
import com.entappia.ei4ostaffprofile.repository.StaffLogsRepository;

@Service
public class LogEvents {

	@Autowired
	StaffLogsRepository staffLogsRepository;

	public synchronized void addLogs(LogEventType type, String major, String minor, String message) {
		if (staffLogsRepository != null) {
			StaffLogs logs = new StaffLogs();
			logs.setDate(new Date());

			if (LogEventType.ERROR == type)
				logs.setType("Error");
			else
				logs.setType("Event");
			logs.setMajor(major);
			logs.setMinor(minor);
			logs.setResult(message);
			staffLogsRepository.save(logs);
		}
	}

}
