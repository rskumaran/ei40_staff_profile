package com.entappia.ei4ostaffprofile.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4ostaffprofile.dbmodels.Tags;
import com.entappia.ei4ostaffprofile.repository.TagsRepository;

@Component
public class TagEvents {

	public static HashMap<String, Tags> tagsMap = new HashMap<>();

	@Autowired
	private TagsRepository tagsRepository;

	public synchronized HashMap<String, Tags> getDBTags() {

		if (tagsMap == null) {
			tagsMap = new HashMap<>();
		}

		if (tagsRepository != null)
			tagsRepository.findAll().forEach(e -> tagsMap.put("" + e.getMacId(), e));

		return tagsMap;
	}

	public void updateTags(List<String> tagIdList) {
		// TODO Auto-generated method stub

		try {
			List<Tags> tagList = new ArrayList<>();

			for (String tagId : tagIdList) {

				Tags tags = new Tags();
				tags.setMacId(tagId);
				tags.setAssignmentId("");
				tags.setAssignmentDate(null);
				tags.setStatus("Free");
				tags.setType("");
				tags.setTagId(tagId);
				tags.setSubType("");
				tags.setManufacturer("");

				tags.setCreatedDate(new Date());
				tags.setModifiedDate(new Date());
				tags.setRestart(false);
				tags.setRenamed(false);
				tags.setMajor(0);
				tags.setMinor(0);

				tagList.add(tags);
			}

			if (tagList.size() > 0) {
				tagsRepository.saveAll(tagList);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
