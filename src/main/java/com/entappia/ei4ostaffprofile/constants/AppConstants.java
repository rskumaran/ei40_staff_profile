package com.entappia.ei4ostaffprofile.constants;

public class AppConstants {
	
	public static String CONFIG_TTA = "Transaction Table Age";
	public static String CONFIG_LTA = "Logger Table Age";
	public static String CONFIG_ARC = "Alert Repeat Count";
	public static String CONFIG_ARI = "Alert Repeat Interval";
	public static String CONFIG_STF = "Staff data from Quuppa";

	public static boolean print_log = false;

public static enum AssignmentStatus {
		NOT_ASSIGNED, ASSIGNED
	}

	public static enum AssignmentType {
		 PERSON, DEPT, TAG
	}

	public static enum ZoneType {
		Trackingzone, DeadZone
	}

	public static enum LogEventType {
		EVENT, ERROR
	}

	public static enum NotificationType {
		INFORMATION, WARNING, EMERGENCY
	}

	public static final int intervalbetweenNotifications = 3;
	public static final int maxNotificationCount = 3;
	//This timing is when a notification is sent upto maxNotification count
	//Then the Notification sent will be available in buffer for a long time
	//It needs to be removed and the buffer needs to be cleared it is kept default as
	//one day.
	public static final int minutesToKeepPreviousNotification = 1440;//one day
	
	public static final int maxDaysToKeepOldTables = 180;//analysis tables older than specified days  are deleted
	public static final int minDaysToKeepOldTables = 10;
	public static final int threadTimeDuration = 1;//its in minutes if needed in seconds need to change logic
	public static final int maxDaysToKeepOldLogData = 180;//Log Data older than specified days  are deleted
	public static final int minDaysToKeepOldLogData = 10;
	
	public static final String timeZone = "Asia/Kolkata";
	
	
	public static final String seckretKey = "Ei4.0123!!!";

	
	public static final String request = "Request";
	public static String success = "Success";
	public static String errormsg = "Error in process";
	
	public static final String session_time = "session_time";
	public static final String sessiontimeout = "Session timed-out or Invalid";
	public static final String fieldsmissing = "Mandatory fields are missing";
	
	

	// Quuppa Apis
	public static String GET_QPE_INFO = "/qpe/getPEInfo?version=2";
	public static String GET_PROJECT_INFO = "/qpe/getProjectInfo?version=2";
	public static String GET_TAG_URL = "/qpe/getTagData?version=2&maxAge=60000&radius=10";
	public static String GET_LOCATOR_INFO = "/qpe/getLocatorInfo?version=2";

}

