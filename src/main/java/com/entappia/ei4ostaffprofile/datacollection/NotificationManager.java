package com.entappia.ei4ostaffprofile.datacollection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4ostaffprofile.constants.AppConstants;
import com.entappia.ei4ostaffprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4ostaffprofile.udp.client.UdpClient;
import com.entappia.ei4ostaffprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4ostaffprofile.utils.Utils;
import com.entappia.ei4ostaffprofile.dbmodels.Notifications;
import com.entappia.ei4ostaffprofile.models.NotificationDetailMessages;
import com.entappia.ei4ostaffprofile.repository.NotificationsRepository;

@Component
public class NotificationManager {
	
	private UdpClient mUdpClient = null;
	@Autowired
	private NotificationsRepository notificationRepository;
	
	ArrayList<Notification> notificationList = new ArrayList<>();
	
	int intervalBetweenNotifications = AppConstants.intervalbetweenNotifications;//Default in minutes
	int maxNotificationCount = AppConstants.maxNotificationCount;// Default
	int minutesToKeepPreviousNotification = AppConstants.minutesToKeepPreviousNotification;//Default
	
	/*@Autowired
	public NotificationManager() {
	}*/
	@Autowired
	public NotificationManager(UdpIntegrationClient udpClient) {
		this.mUdpClient = udpClient;
		Utils.printInConsole("Inside Notification Manager Constructor");
	}
	
	public void setIntervalBetweenNotifications(int intervalBetweenNotifications) {
		this.intervalBetweenNotifications = intervalBetweenNotifications;
	}


	public void setMaxNotificationCount(int maxNotificationCount) {
		this.maxNotificationCount = maxNotificationCount;
	}

	

	public void addNotification(String tagId, String assignedId, String sNotification, NotificationType errorType, String percentage, String zoneName)
	{
		boolean notificationFoundInList =  false;
		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getAssignedId().equals(assignedId) && notification.getTagId().equals(tagId) 
					&& notification.getNotification().equals(sNotification)) {
				notificationFoundInList = true;
				notification.setErrorType(errorType);
				notification.setLastOccuredTime(new Date());
				notification.setSendNotification(true);
				notification.setPercentage(percentage);
				notification.setZoneName(zoneName);
				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications
				
			}
			
		}
		if(notificationFoundInList == false)
		{
			Notification notification = new Notification();
			notification.setAssignedId(assignedId);
			notification.setTagId(tagId);
			notification.setNotification(sNotification);
			notification.setErrorType(errorType);
			notification.setLastOccuredTime(new Date());
			notification.setSendNotification(true);
			notification.setPercentage(percentage);
			notification.setZoneName(zoneName);
			notificationList.add(notification);
		}
		
	}
	public void removeNotification(String tagId,String assignedId,String sNotification)
	{
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getAssignedId().equals(assignedId) && notification.getTagId().equals(tagId) 
					&& notification.getNotification().equals(sNotification)) {
				
				notificationList.remove(i);
			}
			
		}
	}
	public void removeAllNotification(String sNotification)
	{
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getNotification().equals(sNotification)) {
				
				notificationList.remove(i);
			}
			
		}
	}
	public void sendNotification()
	{
		
		List<HashMap<String, String>> lnotificationList = new ArrayList<>(); 
		
		List<HashMap<String, String>> lnotificationZoneList = new ArrayList<>();
		
		List<HashMap<String, String>> lnotificationEmployeeList = new ArrayList<>();
		
		List<HashMap<String, String>> lnotificationTagsList = new ArrayList<>();
		
		NotificationType notificationType = NotificationType.INFORMATION;
		
		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);
			
			if(notification.isSendNotification() == true) {
				
				//check here for time expire and count for sending notification
				
				if(canNotify(notification) ==  true ) {
					
					notification.setSendNotification(true);
					notification.setLastNotifiedTime(new Date());
					notification.setNotificationCount(notification.getNotificationCount() + 1);
					
					HashMap<String, String> notificationmap = new HashMap<>();
					
					if(notification.getNotification().equalsIgnoreCase("crowdedZone")){
						
						notificationmap.put("zoneName", notification.getZoneName());
						notificationmap.put("percentage", notification.getPercentage());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						lnotificationZoneList.add(notificationmap); 
						
					}/*else if(notification.getNotification().equalsIgnoreCase("employeePrimaryLocationCrossed")){
						
						notificationmap.put("zoneName", notification.getZoneName());
						notificationmap.put("employeeId", notification.getAssignedId());
						notificationmap.put("tagId", notification.getTagId());
						lnotificationEmployeeList.add(notificationmap); 
						
					}*/else if(notification.getNotification().equalsIgnoreCase("tags")){
						
						notificationmap.put("zoneName", notification.getZoneName());
						notificationmap.put("tagStatus", notification.getAssignedId());
						notificationmap.put("tagName", notification.getTagId());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						Date currentDate = new Date();
						SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm");
						String scurrentDate = formatter.format(currentDate);
						notificationmap.put("time", scurrentDate );
						lnotificationTagsList.add(notificationmap); 
						
					}else
					{
						//not used now need to change design and clean it later
						notificationmap.put("name", notification.getAssignedId());
						notificationmap.put("status", notification.getNotification());
						notificationmap.put("id", notification.getTagId());
						notificationmap.put("notificationType", notification.getErrorType().toString());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						
						lnotificationList.add(notificationmap);
					}
					
										
					if( notification.getErrorType().compareTo(notificationType) > 0) {
						notificationType = notification.getErrorType();
					}
				}
				
								
				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications
				
			}
			else
			{
				notification.setSendNotification(false);
			}
			
		}
		//Do notification messages checking and send notification in separate list
		//Need to change design soon for now send for testing
		
		if (lnotificationZoneList.size() > 0) {
			
			JSONArray mJSONArray = new JSONArray(lnotificationZoneList);
			JSONObject jsonObject = new JSONObject();
			//jsonObject.put("status", notificationType.name());
			jsonObject.put("status", 200);
			jsonObject.put("type", "crowdedZone");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());
			
			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("crowdedZone");
			notificationDetailMessages.setData(lnotificationZoneList);
			
			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("staff_profile");
			if(lnotificationZoneList.size()>1)
			{
				notifications.setMessage(lnotificationZoneList.size()+" crowed zones");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  
				notifications.setMessage("Crowded Zone:" + lnotificationZoneList.get(0).get("zoneName") +
						"(" + lnotificationZoneList.get(0).get("percentage") + "%)");
				notifications.setMessageObj(null);
			}
			
			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		/*if (lnotificationEmployeeList.size() > 0) {
			
			JSONArray mJSONArray = new JSONArray(lnotificationEmployeeList);
			JSONObject jsonObject = new JSONObject();
			//jsonObject.put("status", notificationType.name());
			jsonObject.put("status", 200);
			jsonObject.put("type", "employeePrimaryLocationCrossed");
			jsonObject.put("data", mJSONArray);
			//Remove this comment when we need to send this notification
			//mUdpClient.sendMessage(jsonObject.toString());
			
			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("employeePrimaryLocationCrossed");
			notificationDetailMessages.setData(lnotificationEmployeeList);
			
			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("staff_profile");
			if(lnotificationEmployeeList.size()>1)
			{
				notifications.setMessage(lnotificationEmployeeList.size()+" Employees crossed primary zones");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  
				notifications.setMessage("Employee:" + lnotificationEmployeeList.get(0).get("employeeId") + "Crossed primary zone to :" +
						lnotificationEmployeeList.get(0).get("zoneName"));
				notifications.setMessageObj(null);
			}
			
			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}*/
		if (lnotificationTagsList.size() > 0) {
			
			JSONArray mJSONArray = new JSONArray(lnotificationTagsList);
			JSONObject jsonObject = new JSONObject();
			//jsonObject.put("status", notificationType.name());
			jsonObject.put("status", 200);
			jsonObject.put("type", "tags");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());
			
			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("tags");
			notificationDetailMessages.setData(lnotificationTagsList);
			
			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("staff_profile");
			
			int blockedTagCount = 0;
			int lostTagCount = 0;
			String message = "";
			
			for(int i = lnotificationTagsList.size() - 1; i >= 0;i--)
			{
				if(lnotificationTagsList.get(i).get("tagStatus").equalsIgnoreCase("lost")){
					lostTagCount++;
				}else if(lnotificationTagsList.get(i).get("tagStatus").equalsIgnoreCase("blocked")){
					blockedTagCount++;
				}
			}
			if(lostTagCount > 0)
			{
				message += lostTagCount + " lost ";
			}
			if(blockedTagCount > 0)
			{
				message += blockedTagCount + " blocked ";
			}
			
			if(lnotificationTagsList.size()>1)
			{
				//notifications.setMessage(lnotificationTagsList.size() + " tags found");
				notifications.setMessage(message + " tags found");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  
				notifications.setMessage(lnotificationTagsList.get(0).get("tagStatus") + " tag " + lnotificationTagsList.get(0).get("tagName") + " found in zone " +
						lnotificationTagsList.get(0).get("zoneName"));
				notifications.setMessageObj(null);
			}

			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		removeExpiredNotifications();
	}

	private void removeExpiredNotifications() {
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
			Date lastNotifiedTime = notification.getLastNotifiedTime();
			Date currentTime = new Date();
			long duration = currentTime.getTime() - lastNotifiedTime.getTime();

			long diffMinutes = duration / (60 * 1000) % 60; 
			
			if(diffMinutes > minutesToKeepPreviousNotification) {
				
				notificationList.remove(i);
			}
			
		}
	}

	private boolean canNotify(Notification notification) {
	
		//Check all the possible conditions to send notification and returns
		//whether particular notification can be sent or not
		if(notification.getLastNotifiedTime() != null)
		{
		Date lastNotified = notification.getLastNotifiedTime();
		Date currentDate = new Date();
		long diff = currentDate.getTime() - lastNotified.getTime();
		
		//long diffSeconds = diff / 1000 % 60;  
		long diffMinutes = diff / (60 * 1000) % 60; 
		//long diffHours = diff / (60 * 60 * 1000);
		
		if(notification.getNotificationCount() < maxNotificationCount &&  diffMinutes >= intervalBetweenNotifications)
		{
			return true;
		}
		}
		else 
		{
			return true;
		}
		return false;
	}

	public void setUdpClient(UdpIntegrationClient udpClient) {
		
		this.mUdpClient = udpClient;
	}
	
	

}
