package com.entappia.ei4ostaffprofile.datacollection;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4ostaffprofile.repository.TableUtils;
import com.entappia.ei4ostaffprofile.dbmodels.VisitorAllocation;
import com.entappia.ei4ostaffprofile.dbmodels.ProfileConfiguration;
import com.entappia.ei4ostaffprofile.models.ConfigData;
import com.entappia.ei4ostaffprofile.repository.ProfileConfigurationRepository;
import com.entappia.ei4ostaffprofile.constants.AppConstants;
import com.entappia.ei4ostaffprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4ostaffprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4ostaffprofile.constants.AppConstants.ZoneType;
import com.entappia.ei4ostaffprofile.dbmodels.CampusDetails;
import com.entappia.ei4ostaffprofile.dbmodels.Tags;
import com.entappia.ei4ostaffprofile.dbmodels.ZoneDetails;
import com.entappia.ei4ostaffprofile.dbmodels.Shift;
import com.entappia.ei4ostaffprofile.dbmodels.Staff;
import com.entappia.ei4ostaffprofile.dbmodels.StaffLogs;
import com.entappia.ei4ostaffprofile.quuppa.QuuppaApiService;
import com.entappia.ei4ostaffprofile.repository.CampusDetailsRepository;
import com.entappia.ei4ostaffprofile.repository.TagsRepository;
import com.entappia.ei4ostaffprofile.repository.VisitorAllocationRepository;
import com.entappia.ei4ostaffprofile.repository.StaffLogsRepository;
import com.entappia.ei4ostaffprofile.repository.ZoneRepository;
import com.entappia.ei4ostaffprofile.repository.ShiftRepository;
import com.entappia.ei4ostaffprofile.repository.StaffRepository;
import com.entappia.ei4ostaffprofile.udp.client.UdpClient;
import com.entappia.ei4ostaffprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4ostaffprofile.utils.LogEvents;
import com.entappia.ei4ostaffprofile.utils.Preference;
import com.entappia.ei4ostaffprofile.utils.Utils;
import com.google.gson.Gson;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;

@Component
public class TagScheduleAsyncTask {

	public String TAG_NAME = "TagScheduleTask";

	Preference preference = new Preference();

	@Autowired
	TableUtils tableUtils;
	
	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;
	
	@Autowired
	private LogEvents logEvents;
	
	@Autowired
	private ShiftRepository shiftRepository;
	
	@Autowired
	private StaffRepository staffRepository;
	
	@Autowired
	private VisitorAllocationRepository visitorAllocationRepository;
	
	@Autowired
	private StaffLogsRepository staffLogsRepository;
	

	private final UdpClient udpClient;

	@Autowired
	private NotificationManager notificationManager ;
	
	//move these functions to a separate class
	//
	private HashMap<String, List<String>> gridZoneMap;
	private JSONArray tagsJsonArray = null;
	private CampusDetails campusDetails = null;
	private JSONArray smoothedPosition = null;
	
	int maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
	int threadTimeDuration = AppConstants.threadTimeDuration;
	int maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
	
	LocalDate tablesDeletedDate = null;
	boolean deleteOldTables = true;
	LocalDate logsDeletedDate = null;
	boolean deleteOldLogs = true;
	
	private ScheduledExecutorService scheduler;
	private ScheduledFuture<?> futureTask = null;
	private Runnable myTask;
	
	@Autowired
	public TagScheduleAsyncTask(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;
	}

	@PostConstruct
	private void intializeThread()
	{
		scheduler =
				 Executors.newScheduledThreadPool(2);
		
		myTask = new Runnable()
		{
			@Override
			public void run()
			{
				getTagPositions();
			}
		};
		if(threadTimeDuration > 0)
	    {       
	        if (futureTask != null)
	        {
	            futureTask.cancel(true);
	        }

	        futureTask = scheduler.scheduleAtFixedRate(myTask, 0, threadTimeDuration * 60, TimeUnit.SECONDS);
	    }
		
	}
	int getReadInterval()
	{
		//read here from repository for changes in thread timing and return
		//need to check if 0 the thread has to be stopped or not
		ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
		int threadTimeDurationLocal = -1;
		if(null != profileConfiguration) {

			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {
						 if (configData.getName().equals(AppConstants.CONFIG_STF)) {
							threadTimeDurationLocal = Integer.parseInt(configData.getDefaultval());
							if(threadTimeDurationLocal <= 0)
							{
								threadTimeDurationLocal = -1;
							}
							Utils.printInConsole("ThreadTimeDuration : "+ threadTimeDurationLocal);
						}
					}
				}
			}
		}

	
		return threadTimeDurationLocal;
	}
	public void checkAndChangeReadInterval()
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {
			public void run() {
				int currentReadInterval = getReadInterval();
				Utils.printInConsole("current thread Interval" + currentReadInterval);
				if(-1 != currentReadInterval) {
					if(currentReadInterval != threadTimeDuration) {
						threadTimeDuration = currentReadInterval;
						changeReadInterval(threadTimeDuration * 60);
					}
				}
			}
		});
		executorService.shutdown();
	}
	public void changeReadInterval(long time)
	{
	    if(time > 0)
	    {       
	        if (futureTask != null)
	        {
	            futureTask.cancel(true);
	        }

	        futureTask = scheduler.scheduleAtFixedRate(myTask, time, time, TimeUnit.SECONDS);
	    }
	}

	Gson gson = new Gson();
	DecimalFormat df = new DecimalFormat("#.##");
	
	private boolean getTagListFromDevice()
	{
		try {
			tagsJsonArray = null;
		CompletableFuture<JSONObject> getTagCompletableFuture = quuppaApiService.getTagDetails();
		CompletableFuture.allOf(getTagCompletableFuture).join();

		if (getTagCompletableFuture.isDone()) {
			preference.setLongValue("lastscan", System.currentTimeMillis());

			JSONObject jsonObject = getTagCompletableFuture.get();
			if (jsonObject != null) {

				String status = jsonObject.optString("status");

				if (!Utils.isEmptyString(status)) {
					if (status.equals("success")) {

						JSONObject jsonTagObject = jsonObject.optJSONObject("response");
						if (jsonTagObject != null) {
							int code = jsonTagObject.getInt("code");
							if (code == 0) {
								tagsJsonArray = jsonTagObject.getJSONArray("tags");
							}
						}
					}
					else if (status.equals("error")) {
						String message = jsonObject.getString("message");
						logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", message);

					}
				}
			}
		}
		}
		catch(Exception e) {
			logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", e.getMessage());
			e.printStackTrace();
		}
		if(tagsJsonArray!= null)
			return true;
		return false;
	}
	private boolean getCampusDetails() {
		campusDetails = null;
		campusDetails = getCampusDetails(1);
		if(campusDetails != null) {
			return true;
		}
		return false;
	}
	private boolean searchTag(String macId) {
		smoothedPosition = null;
		for(int i=0; i<tagsJsonArray.length(); i++){
			JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
			String id = tagJsonObject.optString("tagId", "");
			
			if(macId.equals(id))
			{
				smoothedPosition = tagJsonObject
						.optJSONArray("location");
				if (smoothedPosition != null)
					return true;
				else
					return false;
			}
		}
	
		return false;
	}
	private double getSmoothedPositionX() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(0);
		return -1;
	}
	private double getSmoothedPositionY() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(1);
		return -1;
	}
	private boolean getGridZoneMap() {
		
		gridZoneMap = new HashMap<>();
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();
		else
			return false;
		if(gridZoneMap!= null) {
			return true;
		}
		return false;
	}
	private double getOriginX() {
		if(campusDetails!= null)
			return campusDetails.getOriginX()
				* campusDetails.getMetersPerPixelX();
		return -1;
	}
	private double getOriginY() {
		if(campusDetails!= null)
			return campusDetails.getOriginY()
				* campusDetails.getMetersPerPixelY();
		return -1;
	}
	//@Scheduled(cron = "0/10 * * * * *")
	//@Scheduled(cron = "0 0/1 * * * *")
	
	public void getTagPositions() {
		
				Calendar currentCalendar = Calendar.getInstance();
				currentCalendar.set(Calendar.MILLISECOND, 0);
				currentCalendar.set(Calendar.SECOND, 0);
		 		Date currentTime = currentCalendar.getTime();
				
		Utils.printInConsole("Staff Run Time:-" + Utils.getFormatedDate2(new Date()), true);

		
				ArrayList<Tags> employeeTagList = new ArrayList<Tags>();
				ArrayList<Tags> allLostTagsList = new ArrayList<Tags>();
				ArrayList<Tags> allTagsList = new ArrayList<Tags>();

				double originX = getOriginX();
				double originY = getOriginY();
				List<HashMap<String, String>> statusList = new ArrayList<>();

				try {
					//fetch all tags allocated or mapped to staff or employee from server/DB
					//fetch from QPE one time for all the iterations
					//fetch from qpe
					Utils.printInConsole("GetTaglistfromdevice");
					if(!getTagListFromDevice()) {
						Utils.printInConsole("taglist from device not found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
								"cant get tag list from device");
						return;
					}
					
					Utils.printInConsole("GetCampusdetails");
					if(!getCampusDetails()) {
						Utils.printInConsole("Campusdetails Not Found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
								"Campus Details not found");
						return;
					}
					Utils.printInConsole("GetGridzonemap");
					if(!getGridZoneMap()){
						Utils.printInConsole("Gridzone map Not Found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
								"Grid zone map not found");
						return;
					}
					Utils.printInConsole("GetOriginX");
					originX = getOriginX();
					if(originX == -1) {
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
								"originX not Found ");
						return;
					}
					Utils.printInConsole("GetOriginY");
					originY = getOriginY();
					if(originY == -1) {
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
								"originX not Found");
						return;
					}
					// Get all employee assigned tags 
					//to do more conditions need to be added 
					//check it later
					employeeTagList = (ArrayList<Tags>) tagsRepository.selectTags("Assigned","Employee");

					//all lost and not working tag are fetched here from tag table
					allLostTagsList = (ArrayList<Tags>) tagsRepository.findLostAndBlockedTags();

					//when lost are not working tag sends signal
					//The restart flag to respective tag row is set to true
					//The flag is processed at server at server
					//NO notification is send here need to add one
					//This functionality should run irrespective of employee
					//So it should always run once when the thread starts
					for(int x = 0;x < allLostTagsList.size(); x++)
					{
						double smoothedPositionx = -1;
						double smoothedPositiony= -1;

						Tags LostTag = allLostTagsList.get(x);
						if(searchTag(LostTag.getMacId())== true)
						{
							tagsRepository.setRestartFlag(LostTag.getMacId());


							smoothedPositionx = getSmoothedPositionX();
							smoothedPositiony = getSmoothedPositionY();

							smoothedPositionx = Math.abs(originX - smoothedPositionx);
							smoothedPositiony = Math.abs(originY - smoothedPositiony);

							ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, smoothedPositionx, smoothedPositiony);

							if (zoneDetails != null)
							{
								notificationManager.addNotification(String.valueOf(LostTag.getTagId()), LostTag.getStatus(), "tags",
										NotificationType.EMERGENCY, "", zoneDetails.getName());

								logEvents.addLogs(LogEventType.EVENT, "Run", "Run-StaffprofileApplication",
										"Lost or blocked tag detected MacId: " + LostTag.getMacId());
										//"Lost or not working or expired tag detected MacId: " + LostTag.getMacId());
							}else {
								notificationManager.removeNotification(String.valueOf(LostTag.getTagId()), LostTag.getStatus(), "tags");
							}
						}else {
							notificationManager.removeNotification(String.valueOf(LostTag.getTagId()), LostTag.getStatus(), "tags");
						}
					}
					Utils.printInConsole("lostTagsListSize :" + allLostTagsList.size());
					if(allLostTagsList.size() == 0) {
						notificationManager.removeAllNotification("tags");
					}
					//Get current shift name
					//If there are no active shifts the shift name 
					//will be empty. quit the process or loop when no shift is 
					//found make this function call to the start of the 
					//thread and get out complete thread execution
					Utils.printInConsole("GetCurrentShiftname");
					String currentShiftName = getCurrentShiftName(currentTime);
					if(currentShiftName == "") {
						Utils.printInConsole("No shift available for current time");
						logEvents.addLogs(LogEventType.EVENT, "Run", "Run-StaffprofileApplication",
								"No shift available for current time");
						currentShiftName = "NoShift";
						//comment this return when data need to be collected during no shift time also
						return;
		
					}

					Utils.printInConsole(currentShiftName);
					//append date and shift name as per table strategy
					Utils.printInConsole("GetCurrentTablename");
					String currentTableName = GetCurrentTableName(currentShiftName);
					if(currentTableName == "") {
						Utils.printInConsole("Table Name Formatting error");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
								"Table Name formatting error");
						return;
					}
					Utils.printInConsole(currentTableName);
					//Check whether table exists
					//Create new table if not exists
					Utils.printInConsole("CreateStaffLocationtable");
					tableUtils.createLocationTable(currentTableName);



			
					//HashMap<String, Tags> tagsMap = tagEvents.getDBTags();
					//Get all other values required for notification etc
					//loop for all staff  and visitor tags

					//Occupancy test code Start
					allTagsList = (ArrayList<Tags>) tagsRepository.selectVisitorAndEmployeeTags("Assigned");
					HashMap<Integer, ZoneOccupancy> checknotifications = new HashMap<Integer, ZoneOccupancy>();
					for (int i = 0;i < allTagsList.size();i++){

						Tags tag = allTagsList.get(i); 

						double x = -1;
						double y = -1;

						boolean employeeTag = false;
						boolean visitorTag = false;

						if(searchTag(tag.getMacId())) {

							x = getSmoothedPositionX();
							y = getSmoothedPositionY();

							x = Math.abs(originX - x);
							y = Math.abs(originY - y);

							//remove old notification if everthing is normal
							//notificationManager.removeNotification(tag.getTagId(), tag.getAssignmentId(), "Tag location not found");
						}
						else { 
							//tag not found
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
									"Tag : " + tag.getTagId() + " assigned to an Visitor or employee :" + tag.getAssignmentId() + " is missing" );
							Utils.printInConsole("tag Not Found");
							//notify
							//notificationManager.addNotification(tag.getTagId(), tag.getAssignmentId(), "Tag location not found", NotificationType.WARNING);
							continue;
						}
						Utils.printInConsole("GetZoneDetails");
						//check tag exists
						ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);

						if (zoneDetails != null)
						{
							ZoneType currentZoneType = zoneDetails.getZoneType();
							String zoneName = zoneDetails.getName();


							if(currentZoneType == ZoneType.DeadZone)
							{
								Utils.printInConsole("Tag assigned is in Dead Zone");
								continue;
							}
							Utils.printInConsole("Tag assigned to type: " + tag.getType());

							if(tag.getType().equalsIgnoreCase("Employee") == true)
							{
								Staff AssignedStaff = staffRepository.getStaffByempId(tag.getAssignmentId());

								if(AssignedStaff.isActive() == true || (AssignedStaff.getStatus().equalsIgnoreCase("Working") == true) )
								{
									Utils.printInConsole("Tag is assigned to an Employee not working");
									/*logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
										"Tag : " + staffTag.getTagId() + " is assigned to an Employee :" + staffTag.getAssignmentId() + " who not working" );*/
									employeeTag = true;
								}


							}
							else// if(tag.getType().equalsIgnoreCase("Vendor") || tag.getType().equalsIgnoreCase("Visitor") || tag.getType().equalsIgnoreCase("Contractor"))
							{
								VisitorAllocation visitorAllocation = visitorAllocationRepository.findVisitorAllocation(Integer.parseInt(tag.getAssignmentId()),
										tag.getMacId());
								if(visitorAllocation != null) {
									visitorTag = true;
								}
							}

							if(checknotifications.containsKey(zoneDetails.getId())) {

								ZoneOccupancy  zoneOccupancy = checknotifications.get(zoneDetails.getId());

								if(employeeTag)
									zoneOccupancy.setEmployeeCount(zoneOccupancy.getEmployeeCount() + 1);
								else if (visitorTag)
									zoneOccupancy.setVisitorCount(zoneOccupancy.getVisitorCount() + 1);
								else
									continue;//for loop continue with next iteration

								zoneOccupancy.setZoneMaxCapacity(zoneDetails.getCapacity());
								zoneOccupancy.setZoneCurrentPersons(zoneOccupancy.getZoneCurrentPersons() + 1);
								zoneOccupancy.setZoneID(zoneDetails.getId());
								zoneOccupancy.setZoneName(zoneDetails.getName());
								//update key
							}
							else {
								//Create new key
								ZoneOccupancy  zoneOccupancy = new ZoneOccupancy();

								if(employeeTag)
									zoneOccupancy.setEmployeeCount(1);
								else if (visitorTag)
									zoneOccupancy.setVisitorCount(1);
								else
									continue;//for loop continue with next iteration

								zoneOccupancy.setZoneMaxCapacity(zoneDetails.getCapacity());
								zoneOccupancy.setZoneCurrentPersons(1);
								zoneOccupancy.setZoneID(zoneDetails.getId());
								zoneOccupancy.setZoneName(zoneDetails.getName());

								checknotifications.put(zoneDetails.getId(), zoneOccupancy);

							}


						}

					}
					for ( Integer key : checknotifications.keySet() ) {
						ZoneOccupancy  zoneOccupancy = checknotifications.get(key);
						if(zoneOccupancy != null) {
							double percentage = (zoneOccupancy.zoneCurrentPersons / zoneOccupancy.zoneMaxCapacity) * 100;

							/*Utils.printInConsole("Zone Name : " + zoneOccupancy.getZoneName() + " MaxCap: " + zoneOccupancy.getZoneMaxCapacity()+
									"Ava : " + zoneOccupancy.getZoneCurrentPersons() + " empcount: " + zoneOccupancy.getEmployeeCount() +
									"vistorcount: " + zoneOccupancy.getVisitorCount());*/

							if (percentage >= 80) {
								Utils.printInConsole(zoneOccupancy.getZoneName() + " Zone capacity is " + percentage + "%");
								//notify
								notificationManager.addNotification(String.valueOf(zoneOccupancy.getZoneID()), zoneOccupancy.getZoneName(), "crowdedZone",
										NotificationType.WARNING, String.valueOf(percentage), zoneOccupancy.getZoneName());
							}
							else
							{
								//notify
								notificationManager.removeNotification(String.valueOf(zoneOccupancy.getZoneID()),zoneOccupancy.getZoneName(), "crowdedZone");
							}
						}
					}
					//Occupancy test code End




					Utils.printInConsole("Select Tags");
					//tagList = (ArrayList<Tags>) tagsRepository.selectTags("Assigned","Staff");
					employeeTagList = (ArrayList<Tags>) tagsRepository.selectTags("Assigned","Employee");

					for (int i = 0;i < employeeTagList.size();i++){

						Tags staffTag = employeeTagList.get(i);

						Staff AssignedStaff = staffRepository.getStaffByempId(staffTag.getAssignmentId());

						if(AssignedStaff.isActive() == false || (AssignedStaff.getStatus().compareToIgnoreCase("Working") != 0) )
						{
							Utils.printInConsole("Tag is assigned to an Employee not working");
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
									"Tag : " + staffTag.getTagId() + " is assigned to an Employee :" + staffTag.getAssignmentId() + " who not working" );
							//notify
							//notificationManager.addNotification(String.valueOf(staffTag.getTagId()), 
							//staffTag.getAssignmentId(), "Assigned tag found for employee not working", NotificationType.WARNING);
							//continue;
						}
						else
						{
							//notify
							//notificationManager.removeNotification(String.valueOf(staffTag.getTagId()), 
							//staffTag.getAssignmentId(), "Assigned tag found for employee not working");
						}

						double x = -1;
						double y = -1;


						if(searchTag(staffTag.getMacId())) {

							x = getSmoothedPositionX();
							y = getSmoothedPositionY();

							x = Math.abs(originX - x);
							y = Math.abs(originY - y);

							//notify
							//notificationManager.removeNotification(staffTag.getTagId(), staffTag.getAssignmentId(), "Tag location not found");
						}
						else { 
							//tag not found
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
									"Tag : " + staffTag.getTagId() + " assigned to an Employee :" + staffTag.getAssignmentId() + " is missing" );
							Utils.printInConsole("tag Not Found");
							//notify
							//notificationManager.addNotification(staffTag.getTagId(), staffTag.getAssignmentId(), "Tag location not found", NotificationType.WARNING);
							//return;
						}
						Utils.printInConsole("GetZoneDetails");
						//check tag exists
						ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);
						Utils.printInConsole("GetZoneDetails x: " + x + " y: " + y);
						if (zoneDetails != null)
						{
							ZoneType currentZoneType = zoneDetails.getZoneType();
							String zoneName = zoneDetails.getName();


							if(currentZoneType == ZoneType.DeadZone){
								Utils.printInConsole("Tag assigned is in Dead Zone");
								//notificationManager.addNotification(staffTag.getTagId(), staffTag.getAssignmentId(), "Tag assigned is in Dead Zone", NotificationType.INFORMATION);
								//continue;
							}
							else{
								//notificationManager.removeNotification(staffTag.getTagId(), staffTag.getAssignmentId(), "Tag assigned is in Dead Zone");
							}

							if( !zoneName.equals(AssignedStaff.getLocation1()) && !zoneName.equals(AssignedStaff.getLocation2()) &&
									!zoneName.equals(AssignedStaff.getLocation3()) && currentZoneType != ZoneType.DeadZone)
							{
								Utils.printInConsole("Employee out of his specified areas");
								logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
										"Employee is in zone :" + zoneName + "which is not assigned to him"  );
								//Notify
								//notificationManager.addNotification(staffTag.getTagId(), staffTag.getAssignmentId(), "employeePrimaryLocationCrossed", 
										//NotificationType.WARNING, "", zoneName);

							}
							else
							{
								if(currentZoneType != ZoneType.DeadZone) {
									//remove old notification if everthing is normal
									//notificationManager.removeNotification(staffTag.getTagId(), staffTag.getAssignmentId(), "employeePrimaryLocationCrossed");
								}
								else {
									logEvents.addLogs(LogEventType.EVENT, "Run", "Run-StaffprofileApplication",
											"Employee is in Non Tracking Zone (Dead zone)" );
								}
							}
							if (zoneDetails != null && currentZoneType != ZoneType.DeadZone) {
								
								/*Utils.printInConsole("Time in Minutes : " + Utils.getDayMintus(currentDate));
								Utils.printInConsole("Staff ID : " + staffTag.getAssignmentId());
								Utils.printInConsole("Zone ID : " + zoneDetails.getId());
								Utils.printInConsole("Zone Name : " + zoneName);
								Utils.printInConsole("Grid xPosition : " + getGridPosition(x));
								Utils.printInConsole("Grid yposition : " + getGridPosition(y));
								Utils.printInConsole("Grid Number : " + ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y)));
								*/

								int GridNumber = (int) ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y));

								int xValue = getGridPosition(x);
								int yValue = getGridPosition(y);

								String gridPosition = xValue + "_" + yValue;

								String staffTagAssignmentID = staffTag.getAssignmentId();
								int zoneId = zoneDetails.getId();
								int currentTimeinMinutes = Utils.getDayMintus(currentTime);

								tableUtils.insertIntoLocationTable(currentTableName, staffTagAssignmentID, currentTimeinMinutes,
										GridNumber, zoneId,zoneName, gridPosition, threadTimeDuration);
								/*insertIntoStaffLocationTable(String tableName, String staffID, int dayTimeInMinutes, int gridNumber, 
									String zoneID, String gridPos)*/

							} else {
								//zone details not found
								Utils.printInConsole("zone details Not Found");
								//return;
							}

							//write all the details required to the table
							//send required notification to server through UDP	

						}

						else
						{
							Utils.printInConsole("zone details not found");
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-StaffprofileApplication",
									"ZoneDetails x: " + x + " y: " + y + "is not found" );

						}
					}
				}
				catch(Exception e) {
						e.printStackTrace();
				}

				notificationManager.sendNotification();
				checkAndChangeReadInterval();
				checkAndModifyConfigurationData();
				modifyTableData();
	}

	private void dropExpiredTables()
	{
		
	}
	
	//GetCurrentTableName
	//converts string according to the table name rules
	//need to handle this issue if no shift is found 
	private String GetCurrentTableName(String shiftName){
		String currentTableName = "";
		String profileName = "Staff_Location";
		Date currentDate = new Date();
		
		//Table name hours and minutes can be used to find out in case of issues
		//For only date is used we can change it later
		//SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
	    String scurrentDate = formatter.format(currentDate);
	    
	    currentTableName = profileName + "_" + scurrentDate + "_" + shiftName;

		return currentTableName;
		
	}
	//GetCurrentShiftName
	//The current working shift is needed to append in the table name 
	//to find the current working shift the current time is checked 
	//whether it lies between shift start time shift end time 
	// the issue occurs when two shifts are running in the same time
	// have to handle it 
	private String getCurrentShiftName(Date currentTime)
	{
		//Get current shift name from the shift start time and 
		ArrayList<Shift> shiftDetailsArr = (ArrayList<Shift>) shiftRepository.findActiveShifts();
		
			
		    String shiftName = "";
			for (int i = 0;i < shiftDetailsArr.size();i++){
				Shift shiftElement = shiftDetailsArr.get(i);
				Date shiftStartTime = shiftElement.getShiftStartTime();
				Date shiftEndTime = shiftElement.getShiftEndTime();
				
				Utils.printInConsole("shiftStartTime" + shiftStartTime);
				Utils.printInConsole("shiftEndTime" + shiftEndTime);
				
				Calendar calendar = Calendar.getInstance();
				calendar.clear();
		        calendar.setTime(shiftStartTime);
		        calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
		        shiftStartTime = calendar.getTime();
		        
		        calendar.clear();
		        calendar.setTime(shiftEndTime);
		    	calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
		        shiftEndTime = calendar.getTime();
		        
		        if(currentTime.after(shiftStartTime) && currentTime.before(shiftEndTime)){
					  shiftName = shiftElement.getShiftName(); break; }
	        if(currentTime.equals(shiftStartTime)){
	    		shiftName = shiftElement.getShiftName();
	    		break;
	    	}
					
			}
		
			return shiftName;
		}
	private ZoneDetails getZoneDetails(HashMap<String, List<String>> gridZoneMap, double x, double y) {

		int xValue = getGridPosition(x);
		int yValue = getGridPosition(y);

		if (gridZoneMap.containsKey(xValue + "_" + yValue)) {

			List<String> zoneIdList = gridZoneMap.get(xValue + "_" + yValue);
			if (zoneIdList != null && zoneIdList.size() > 0) {
				if (zoneIdList.size() == 1) {
					String zoneId = zoneIdList.get(0);
					Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
					if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive())
						return zoneDetails.get();
				} else {
					for (String zoneId : zoneIdList) {
						Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
						if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive()) {

							ZoneDetails zoneDetails1 = zoneDetails.get();
							List<HashMap<String, String>> coordinateList = zoneDetails1.getCoordinateList();
							if (isInsidePolygon(coordinateList, x, y))
								return zoneDetails1;
						}
					}
				}
			}
		}

		return null;
	}

	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	private int getGridPosition(double num1) {
		int value = 0;
		if (num1 % 1 == 0)
			value = (int) Math.floor(num1);
		else
			value = (int) Math.floor(num1 + 1);

		return value - 1;
	}
 
 

	public CampusDetails getCampusDetails(int campusId) {
		return campusDetailsRepository.findByCampusId(campusId);
	}

	public HashMap<String, List<String>> getCampusGridList() {
		//gridZoneMap = new HashMap<>();
		//campusDetails = campusDetailsRepository.findByCampusId(1);
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();

		return gridZoneMap;
	}
	void checkAndModifyConfigurationData() {
		try {
			ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
			if(null != profileConfiguration) {
			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {

						if (configData.getName().equals(AppConstants.CONFIG_TTA))
						{
							maxDaysToKeepOldTables = Integer.parseInt(configData.getDefaultval());
							if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
							{
								if(maxDaysToKeepOldTables != AppConstants.maxDaysToKeepOldTables) {
									deleteOldTables = true;
								}
								if(null == tablesDeletedDate)
								{
									deleteOldTables = true;
								}
								else {//database deleted date not NULL
									LocalDate currentDate = LocalDate.now();
									if(!tablesDeletedDate.isEqual(currentDate)) {
										deleteOldTables = true;
									}
								}

							}
							else
							{
								maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
								deleteOldTables = true;
							}

						}
						else if (configData.getName().equals(AppConstants.CONFIG_LTA))
						{
							maxDaysToKeepOldLogData = Integer.parseInt(configData.getDefaultval());

							if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
							{
								if(maxDaysToKeepOldLogData != AppConstants.maxDaysToKeepOldLogData) {
									deleteOldLogs = true;
								}
								
								if(null == logsDeletedDate)
								{
									deleteOldLogs = true;
								}
								else {//database deleted date not NULL
									LocalDate currentDate = LocalDate.now();
									if(!logsDeletedDate.isEqual(currentDate)) {
										deleteOldLogs = true;
									}
								}
							}
							else
							{
								maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
								deleteOldLogs = true;
							}

						}
						else if (configData.getName().equals(AppConstants.CONFIG_ARI)) 
						{
							int intervalBetweenNotifications = Integer.parseInt(configData.getDefaultval());
							if(intervalBetweenNotifications > 0)
							{
								notificationManager.setIntervalBetweenNotifications(intervalBetweenNotifications);
							}
							Utils.printInConsole("intervalBetweenNotifications : "+ intervalBetweenNotifications);
						}
						else if (configData.getName().equals(AppConstants.CONFIG_ARC)) {
							int maxNotificationCount = Integer.parseInt(configData.getDefaultval());
							if(maxNotificationCount >= 0)
							{
								notificationManager.setMaxNotificationCount(maxNotificationCount);
							}
							Utils.printInConsole("maxNotificationCount : "+ maxNotificationCount);
						}

					}

				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	void modifyTableData(){

		if(deleteOldTables) {
			LocalDate lastDateToKeepLocationTable = getLastDateToKeepLocationTable(maxDaysToKeepOldTables);
			deleteOldLocationTable(lastDateToKeepLocationTable);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldTables : " + maxDaysToKeepOldTables + "Date : "+ lastDateToKeepLocationTable + "deleteOldTables : "+ deleteOldTables);
			deleteOldTables = false;
		}
		if(deleteOldLogs) {
			LocalDate lastDateToKeepLogData = getLastDateToKeepLogData(maxDaysToKeepOldLogData);
			deleteOldLogs(lastDateToKeepLogData);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldLogData : " + maxDaysToKeepOldLogData + "Date : "+ lastDateToKeepLogData + "deleteOldTables : "+ deleteOldLogs);
			deleteOldLogs = false;
		}

	}
	private void deleteOldLogs(LocalDate lastDateToKeepLogData) {

		int queryResult = staffLogsRepository.deleteByDate(lastDateToKeepLogData );
		Utils.printInConsole("Delete Query Result : " + queryResult);
			
		
		List<StaffLogs> Logs = (List<StaffLogs>)staffLogsRepository.selectLogsBeforeDate(lastDateToKeepLogData );
		if(Logs != null)
		{
			if(Logs.size() == 0) {
				Utils.printInConsole("No Records found");
			}
			for(int j=0;j<Logs.size();j++)
			{
				Utils.printInConsole("Date : "+ Logs.get(j).getDate() + "Result : "+ Logs.get(j).getResult());
			}
		}
		else {
			Utils.printInConsole("Logs null");
		}
	}

	private LocalDate getLastDateToKeepLogData(int daysToKeepOldLogData2) {
		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);
		return LastDate;
	}

	@SuppressWarnings("unchecked")
	private void deleteOldLocationTable(LocalDate lastDateToKeepLocationTable) {

		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String lastDate = lastDateToKeepLocationTable.format(formatters);
		List<String> tableNames = (List<String>)tableUtils.SelectTableNamesLike(getStaffTableName(), lastDate );
		if(tableNames != null)
		{
			for(int j=0;j<tableNames.size();j++)
			{
				Utils.printInConsole("In Between chrono Table Names : " + tableNames.get(j) + "last date : "+ lastDateToKeepLocationTable.toString());
				tableUtils.dropLocationTable(tableNames.get(j));
			}
		}
	}

	private LocalDate getLastDateToKeepLocationTable(int maxDaysToKeepOldTables) {

		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);

		return LastDate;
	}
	
	private String getStaffTableName() {
		return getTableName("Staff");
	}

	private String getTableName(String tableName) {
		String currentTableName = "";
		String profileName = tableName + "_Location";

		currentTableName = profileName + "_";

		return currentTableName;
	}
}
