package com.entappia.ei4ostaffprofile.datacollection;

public class ZoneOccupancy {

	int zoneID;
	public int getZoneID() {
		return zoneID;
	}
	public void setZoneID(int zoneID) {
		this.zoneID = zoneID;
	}
	public int getZoneMaxCapacity() {
		return zoneMaxCapacity;
	}
	public void setZoneMaxCapacity(int zoneMaxCapacity) {
		this.zoneMaxCapacity = zoneMaxCapacity;
	}
	public int getZoneCurrentPersons() {
		return zoneCurrentPersons;
	}
	public void setZoneCurrentPersons(int zoneCurrentCapacity) {
		this.zoneCurrentPersons = zoneCurrentCapacity;
	}
	public int getEmployeeCount() {
		return employeeCount;
	}
	public void setEmployeeCount(int employeecount) {
		this.employeeCount = employeecount;
	}
	public int getVisitorCount() {
		return visitorCount;
	}
	public void setVisitorCount(int visitorCount) {
		this.visitorCount = visitorCount;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	int zoneMaxCapacity;
	int zoneCurrentPersons;
	int employeeCount;
	int visitorCount;
	String zoneName;
	
	
}
