package com.entappia.ei4ostaffprofile.datacollection;

import java.util.Date;

import com.entappia.ei4ostaffprofile.constants.AppConstants.NotificationType;

public class Notification {
	
	String tagId;
	String assignedId;
	Date lastNotifiedTime;
	String Notification;
	NotificationType errorType;
	int notificationCount = 0;
	Date lastOccuredTime;
	boolean sendNotification;
	String percentage;
	String zoneName;
	
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public String getAssignedId() {
		return assignedId;
	}
	public void setAssignedId(String assignedId) {
		this.assignedId = assignedId;
	}
	public Date getLastNotifiedTime() {
		return lastNotifiedTime;
	}
	public void setLastNotifiedTime(Date lastNotifiedTime) {
		this.lastNotifiedTime = lastNotifiedTime;
	}
	public String getNotification() {
		return Notification;
	}
	public void setNotification(String notification) {
		Notification = notification;
	}
	public NotificationType getErrorType() {
		return errorType;
	}
	public void setErrorType(NotificationType errorType) {
		this.errorType = errorType;
	}
	public int getNotificationCount() {
		return notificationCount;
	}
	public void setNotificationCount(int notificationCount) {
		this.notificationCount = notificationCount;
	}
	public Date getLastOccuredTime() {
		return lastOccuredTime;
	}
	public void setLastOccuredTime(Date lastOccuredTime) {
		this.lastOccuredTime = lastOccuredTime;
	}
	public boolean isSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}
	

}
