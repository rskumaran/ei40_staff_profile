package com.entappia.ei4ostaffprofile.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4ostaffprofile.constants.AppConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Organization")
public class Organization {

	@Id
	@Column(length = 37)
	String orgId;
	@Column(length = 50)
	String OrgName;
	@Column(length = 37)
	String licenseKey;
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm", timezone= AppConstants.timeZone)
	Date operationStartTime;
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm", timezone= AppConstants.timeZone)
	Date operationEndTime;
	@Column(length = 16)
	String lastName;
	@Column(length = 16)
	String firstName;
	@Column(length = 15)
	String phoneNumber;
	@Column(length = 20)
	String email;
	@Column(length = 50)
	String address;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return OrgName;
	}

	public void setOrgName(String orgName) {
		OrgName = orgName;
	}

	public String getLicenseKey() {
		return licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	public Date getOperationStartTime() {
		return operationStartTime;
	}

	public void setOperationStartTime(Date operationStartTime) {
		this.operationStartTime = operationStartTime;
	}

	public Date getOperationEndTime() {
		return operationEndTime;
	}

	public void setOperationEndTime(Date operationEndTime) {
		this.operationEndTime = operationEndTime;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
