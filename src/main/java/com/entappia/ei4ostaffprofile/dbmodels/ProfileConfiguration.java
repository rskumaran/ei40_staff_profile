package com.entappia.ei4ostaffprofile.dbmodels;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.entappia.ei4ostaffprofile.converters.ConfigurationListConverter;
import com.entappia.ei4ostaffprofile.models.ConfigData;

@Entity
@Table(name = "ProfileConfiguration")
public class ProfileConfiguration {

	@Id
	private String organization;
	@Convert(converter = ConfigurationListConverter.class)
	@Column(name = "configData", columnDefinition = "json") 
	List<ConfigData> configData;
	  
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	 
	public List<ConfigData> getConfigData() {
		return configData;
	}
	public void setConfigData(List<ConfigData> configData) {
		this.configData = configData;
	}
	
	
	 
}
