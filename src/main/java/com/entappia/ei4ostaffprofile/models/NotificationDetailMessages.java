package com.entappia.ei4ostaffprofile.models;

import java.util.HashMap;
import java.util.List;

public class NotificationDetailMessages {

	private String type;
    private List<HashMap<String, String>> data;
    private HashMap<String, List<HashMap<String, String>>> tagData;
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<HashMap<String, String>> getData() {
		return data;
	}
	public void setData(List<HashMap<String, String>> data) {
		this.data = data;
	}
	public HashMap<String, List<HashMap<String, String>>> getTagData() {
		return tagData;
	}

	public void setTagData(HashMap<String, List<HashMap<String, String>>> tagData) {
		this.tagData = tagData;
	}
    
}
