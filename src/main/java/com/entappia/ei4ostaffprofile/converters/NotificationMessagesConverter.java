package com.entappia.ei4ostaffprofile.converters;

import java.lang.reflect.Type;

import javax.persistence.AttributeConverter;

import com.entappia.ei4ostaffprofile.models.NotificationDetailMessages;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class NotificationMessagesConverter implements AttributeConverter<NotificationDetailMessages, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<NotificationDetailMessages>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(NotificationDetailMessages data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public NotificationDetailMessages convertToEntityAttribute(String s) {
		if (null == s || s.equals("{}")) {
			// You may return null if you prefer that style
			return null;
		}

		try {

			NotificationDetailMessages notificationDetailMessages = gson.fromJson(s, mapType);
			return notificationDetailMessages;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
