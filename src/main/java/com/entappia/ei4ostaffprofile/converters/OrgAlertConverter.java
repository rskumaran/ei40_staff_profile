package com.entappia.ei4ostaffprofile.converters;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.entappia.ei4ostaffprofile.models.Alert;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class OrgAlertConverter implements AttributeConverter<HashMap<String, Alert>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<HashMap<String, Alert>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(HashMap<String, Alert> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public HashMap<String, Alert> convertToEntityAttribute(String s) {
		if (null == s || s.equals("{}")) {
			// You may return null if you prefer that style
			return new HashMap<>();
		}

		try {

			HashMap<String, Alert> nameEmployeeMap = gson.fromJson(s, mapType);

			return nameEmployeeMap;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
