package com.entappia.ei4ostaffprofile.converters;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.json.JSONObject;
 
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class HashMapListConverter implements AttributeConverter<HashMap<String, List<String>>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<HashMap<String, List<String>>>() {
	}.getType();
	
	@Override
	public String convertToDatabaseColumn(HashMap<String, List<String>> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public HashMap<String, List<String>> convertToEntityAttribute(String s) {
		if (null == s) {
			// You may return null if you prefer that style
			return new HashMap<>();
		}

		try { 
			HashMap<String, List<String>> nameEmployeeMap = gson.fromJson(s, mapType);

			return nameEmployeeMap;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
