package com.entappia.ei4ostaffprofile.converters;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class ListConverter implements AttributeConverter<List<String>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<List<String>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(List<String> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "[]";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public List<String> convertToEntityAttribute(String s) {
		if (null == s || s.equals("[]")) {
			// You may return null if you prefer that style
			return new ArrayList();
		}

		try {

			List<String> list = gson.fromJson(s, mapType);

			return list;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
