package com.entappia.ei4ostaffprofile.udp.client;

public interface UdpClient {
	public void sendMessage(String message);
}
